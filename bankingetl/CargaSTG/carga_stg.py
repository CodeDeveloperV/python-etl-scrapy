import os
import shutil
import time
import traceback

import pandas
import scrapping_utils


def transform_and_load_data(year, month):
    # ====================== Rutas a utilizar ======================#
    print("Inicia el proceso de ETL")

    scraping_utils = scrapping_utils.ScrappingUtils()
    scraping_utils.generator_paths(year, month)

    # ====================== Inicio del Loop por archivo ======================#
    for file in scraping_utils.filelist:
        if file.endswith(".xlsx"):
            print("Inicia el ciclo para el archivo " + file)
            try:
                errorMain = ''

                # Nombre del archivo
                fecha_creacion = time.strftime('%Y%m%d')
                fecha_subida = time.strftime('%Y%m%d')
                bank_short_name = (file[file.find('-en-') + 4:-5])
                tipo = (file[3:file.find('-BANCO-en-')])
                # print (cl.path_procesados + file)
                # Se elimina el archivo si existe

                filestg1 = scraping_utils.filestg
                if os.path.exists(filestg1):
                    os.remove(filestg1)

        # ----------------------------Limpieza Archivo----------------------------
                print(scraping_utils.path)
                df = scraping_utils.clean_excel(scraping_utils.path, file)
                year, Bank = scraping_utils.df_year(scraping_utils.path, file,
                                                    tipo)
                if os.path.exists(
                    os.path.join(scraping_utils.path_procesados, year, file)):
                    print("Ya fue procesado")
                    continue
                    # ----------------------------Despivoteo----------------------------
                df2 = scraping_utils.unpivot(df, tipo, year, fecha_subida, Bank,
                                             bank_short_name)
                # Cargamos los datos al csv de Staging
                print('se crea archivo temporal')
                df2.to_csv(filestg1, sep='|')
                fecha_creacion = (year + "01" + "01")
    # =============================== CARGA TABLA INICIAL STAGING ==============
                print("Se crea el archivo dentro de la tabla de control")
                # filecontrol_s = ('python '+ cl.scripts +'filecontrol.py ' + file + ' ' + Bank.replace(" ", "_") + ' ' + fecha_creacion + ' ' + fecha_subida + ' ' + 'null' + ' ' + 'null')
                # os.system(filecontrol_s)
                try:
                    print("Copia los datos del csv a la tabla inicial")
                    csvbat = os.system(
                        os.path.join(scraping_utils.path_bat, "copycsv.sh"))
                    print('running "%s"' % csvbat)
                    if csvbat != 1:
                        print("Se cargo con exito el archivo")
                    else:
                        print("ERROR EN EL CSV.BAT")
                        raise ValueError('Hubo un error')
                    try:

                        print("Se ejecutara el bat de lookup")
                        accountlookup = os.system(
                            os.path.join(scraping_utils.path_bat, "accountLookup.sh"))
                        print(accountlookup)
                        shutil.move(os.path.join(scraping_utils.down_path, file),
                                    scraping_utils.path_procesados)
                        estado = 'Succesfull'
                        # filecontrol_s = (
                        #           'python '+cl.path_scripts+'filecontrol.py ' + file + ' ' +  Bank.replace(" ", "_") + ' ' + fecha_creacion + ' ' + fecha_subida + ' ' + estado + ' ' + 'null')
                        # print(filecontrol_s)
                        # os.system(filecontrol_s)
                    except:
                        errcsv = traceback.format_exc()
                        print(errcsv)
                        estado = 'Failed'
                        errorMain = "Fallo el proceso de lookup para el archivo" + file
                        shutil.move(scraping_utils.path + file,
                                    scraping_utils.path_rechazados)

                except ValueError as err:
                    print("Entro a ValueError")
                    errcsv = []
                    errorval = "Hubo un error: "
                    errorcsv = open(scraping_utils.path_log + 'copycsv1.log')
                    flag = True
                    for err in errorcsv:
                        errcsv.append(err)
                        if err.startswith('psql:'):
                            flag = True
                        if flag:
                            errorval = "".join(errcsv)
                            flag = True
                        if err.strip().startswith('CONTEXTO:'):
                            flag = False
                    errorval = errorval.replace('\n', ' ')
                    errorini = errorval.find('ERROR:')
                    errorfin = errorval.find('CONTEXTO:')
                    errorval = errorval[errorini:errorfin]
                    errorval = errorval.replace(':', 'o')
                    errorval = errorval.split()
                    print(errorval)
                    estado = 'Failed'
                    errorMain = "Fallo al intentar subir el archivo " + file
                    # print('python '+cl.path_scripts+'filecontrol.py ' + file + ' ' + Bank.replace(" ", "_") + ' ' + fecha_creacion + ' ' + fecha_subida + ' ' + estado + ' ' + ' '.join(
                    #       errorval))
                    # filecontrol_s = (
                    #           'python '+cl.path_scripts+'filecontrol.py ' + file + ' ' + Bank.replace(" ", "_") + ' ' + fecha_creacion + ' ' + fecha_subida + ' ' + estado + ' ' + ' '.join(
                    #       errorval))
                    # print(filecontrol_s)
                    # os.system(filecontrol_s)

            except Exception as e:
                print(e)
                print("error")
                pass
